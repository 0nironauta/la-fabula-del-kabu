# La Fábula del Kabu

Este es el proyecto original del juego La Fábula del Kabu para Môsi 
(https://zenzoa.itch.io/mosi)

Puedes editarlo libremente y usarlo para hacer tus propios juegos en Môsi, o expandir la historia como tú quieras ¡No olvides pasar por oniria.world para enseñarnos tus progresos!
